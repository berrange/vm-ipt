# SPDX-License-Identifier: LGPL-2.1-or-later
# SPDX-FileCopyrightText: 2023 Red Hat

KDIR = /lib/modules

KVER = $(shell ls $(KDIR) | tail -1)

KIMAGE = $(KDIR)/$(KVER)/vmlinuz

MAKE_TINY_IMAGE = tiny-vm-tools/make-tiny-image.py

BYEBYEBIOS_DIR = byebyebios

BYEBYEBIOS = $(BYEBYEBIOS_DIR)/byebyebios
BYEBYEBIOS_BS = $(BYEBYEBIOS_DIR)/bootstub.bin
BYEBYEBIOS_MSG = $(BYEBYEBIOS_DIR)/nouefi.txt
BYEBYEBIOS_ARGS = --boot-stub $(BYEBYEBIOS_BS) --message $(BYEBYEBIOS_MSG)

MKINITRD_EXTRA = /lib64/libtss2-tcti-device.so.0=/lib64/libtss2-tcti-device.so.0

MKINITRD_COPY = $(MKINITRD_EXTRA:%=--copy %)

MKINITRD = $(MAKE_TINY_IMAGE) --kver $(KVER) --kmod dm_crypt --kmod vfat --kmod virtio_blk --blockdevs $(MKINITRD_COPY)

BUILD_DIR = target

IMAGE_TEMPLATE = $(BUILD_DIR)/dummy.img

IMAGE_FILE = $(BUILD_DIR)/test.img

OVMF_DIR = /usr/share/edk2/ovmf
OVMF_CODE = $(OVMF_DIR)/OVMF_CODE.fd
OVMF_VARS = $(OVMF_DIR)/OVMF_VARS.fd

OVMF_LOCAL_VARS = $(OVMF_VARS:$(OVMF_DIR)/%=$(BUILD_DIR)/%)

SWTPM_SETUP = swtpm_setup
SWTPM = swtpm
SWTPM_STATE = $(BUILD_DIR)/test.tpm
SWTPM_SOCK = $(SWTPM_STATE)/swtpm.sock
SWTPM_LOG = $(SWTPM_STATE)/swtpm.log
SWTPM_ARGS = socket --log file=$(SWTPM_LOG),level=10,truncate --daemon --terminate --tpm2 --tpmstate dir=$(SWTPM_STATE) --ctrl type=unixio,path=$(SWTPM_SOCK)

QEMU_BASE = qemu-system-x86_64 -nodefaults -accel kvm

QEMU_TUI = -display none -serial stdio

GROW_MB = 32

DEBUG = quiet

QEMU_BOOT_TINY_INITRD = -kernel $(KIMAGE) -append "console=ttyS0 $(DEBUG)"
QEMU_BOOT_OVMF = -blockdev file,filename=$(OVMF_CODE),node-name=fwcode,read-only=on -blockdev file,filename=$(OVMF_LOCAL_VARS),node-name=fwvars -machine q35,pflash0=fwcode,pflash1=fwvars

QEMU_STORAGE = -blockdev driver=file,filename=$(IMAGE_FILE),node-name=img0 -device virtio-blk-pci,drive=img0

QEMU_NETWORK = -device virtio-net,netdev=net0 -netdev user,id=net0

QEMU_TPM = -chardev socket,id=ctpm0,path=$(SWTPM_SOCK) -tpmdev emulator,id=tpm0,chardev=ctpm0 -device tpm-tis,tpmdev=tpm0

QEMU_RUN_DIRECT = $(QEMU_BASE) -m 4096 $(QEMU_TUI) $(QEMU_BOOT_OVMF) $(QEMU_BOOT_TINY_INITRD) $(QEMU_STORAGE) $(QEMU_TPM)

QEMU_RUN_IMAGE = $(QEMU_BASE) -m 4096 $(QEMU_TUI) $(QEMU_GUI) $(QEMU_BOOT_OVMF) $(QEMU_STORAGE),bootindex=1 $(QEMU_NETWORK) $(QEMU_TPM)

SFDISK_LAYOUT = "size=50M, type=C12A7328-F81F-11D2-BA4B-00A0C93EC93B\n size=+, type=4F68BCE3-E8CD-4DB1-96E7-FBCAF984B709"

BINARY = $(BUILD_DIR)/debug/imprint

all: $(BINARY)

SOURCE = $(wildcard src/*.rs src/*/*.rs) Cargo.toml

.PHONY = $(BINARY)

$(OVMF_LOCAL_VARS): $(OVMF_VARS)
	cp $< $@

$(SWTPM_STATE):
	mkdir -p $@
	$(SWTPM_SETUP) --create-config-files skip-if-exist
	$(SWTPM_SETUP) --tpm2 --tpmstate $@ --createek --create-ek-cert --create-platform-cert --lock-nvram

$(BINARY): $(SOURCE)
	cargo build --bin $(BINARY:$(BUILD_DIR)/debug/%=%)

target/dummy.img:
	qemu-img create -f raw $@ 100m
	echo -e "label: gpt" | sfdisk $@
	echo -e $(SFDISK_LAYOUT) | sfdisk $@
	echo -e 'run\nmkfs vfat /dev/sda1\nmkfs ext4 /dev/sda2\n mount /dev/sda2 /\n echo "Hello World" > /README.txt\nexit' | guestfish -a $@

$(IMAGE_FILE): $(IMAGE_TEMPLATE) $(BYEBYEBIOS_BS)
	qemu-img convert -O raw $< $@
	$(BYEBYEBIOS) $(BYEBYEBIOS_ARGS) $@

$(MAKE_TINY_IMAGE):
	git clone https://gitlab.com/berrange/tiny-vm-tools

$(BYEBYEBIOS):
	git clone https://gitlab.com/berrange/byebyebios

$(BYEBYEBIOS_BS): $(BYEBYEBIOS)
	cd $(BYEBYEBIOS_DIR) && make

$(BUILD_DIR)/imprint-sign.img: $(BINARY) $(MAKE_TINY_IMAGE) Makefile
	$(MKINITRD) --output $@ --run "mkdir -p /boot/efi && mount /dev/vda1 /boot/efi && imprint sign --root-dev /dev/vda2" $(realpath $<)

$(BUILD_DIR)/imprint-verify.img: $(BINARY) $(MAKE_TINY_IMAGE) Makefile
	$(MKINITRD) --output $@ --run "mkdir -p /run/cryptsetup /boot/efi && mount /dev/vda1 /boot/efi && echo -n '123456' > key.txt && cryptsetup luksOpen --key-file key.txt /dev/vda2 root && imprint verify --root-dev /dev/mapper/root && tpm2 pcrread sha256:15" $(realpath $<) cryptsetup tpm2

$(BUILD_DIR)/imprint-sh.img: $(BINARY) $(MAKE_TINY_IMAGE) Makefile
	$(MKINITRD) --output $@ $(realpath $<) parted strace gdb dd od cryptsetup tpm2

$(BUILD_DIR)/encrypt.img: $(MAKE_TINY_IMAGE) Makefile
	$(MKINITRD) --output $@ --run "mkdir -p /run/cryptsetup && echo -n '123456' > key.txt && echo \"Encrypting volume\" && cryptsetup reencrypt --progress-frequency 1 --progress-json --debug --encrypt -v -q --type luks2 --key-file key.txt --reduce-device-size 32768k /dev/vda2" cryptsetup


run-sign: $(BUILD_DIR)/imprint-sign.img $(IMAGE_FILE) $(OVMF_LOCAL_VARS) $(SWTPM_STATE)
	$(SWTPM) $(SWTPM_ARGS)
	$(QEMU_RUN_DIRECT) -initrd $<

grow: $(IMAGE_FILE)
	qemu-img resize $< +$(GROW_MB)M
	echo -e ", +" | sfdisk -N 2 $<

run-verify: $(BUILD_DIR)/imprint-verify.img $(IMAGE_FILE) $(OVMF_LOCAL_VARS) $(SWTPM_STATE)
	$(SWTPM) $(SWTPM_ARGS)
	$(QEMU_RUN_DIRECT) -initrd $< | tee target/verify.log
	@echo -e "run\nmount /dev/vda1 /\ncopy-out /imprint/signature.json target\nexit" | guestfish -a $(IMAGE_FILE)
	@sha256sum target/signature.json | awk '{print $$1}' > target/signature.json.sha256
	@echo -e "from hashlib import sha256\npcr = bytes([0] * sha256().digest_size)\npcr = sha256(pcr + bytes.fromhex('`cat target/signature.json.sha256`')).hexdigest()\nprint(f\"Expect PCR 15: 0x{pcr}\")" | python3 -
	@echo -n "Actual PCR "
	@grep 15: target/verify.log | sed -e 's/^\s*//' | tr A-Z a-z

run-shell: $(BUILD_DIR)/imprint-sh.img $(IMAGE_FILE) $(OVMF_LOCAL_VARS) $(SWTPM_STATE)
	$(SWTPM) $(SWTPM_ARGS)
	$(QEMU_RUN_DIRECT) -initrd $<

run-encrypt: $(BUILD_DIR)/encrypt.img $(IMAGE_FILE) $(OVMF_LOCAL_VARS) $(SWTPM_STATE)
	$(SWTPM) $(SWTPM_ARGS)
	$(QEMU_RUN_DIRECT) -initrd $<

run-vm: $(IMAGE_FILE) $(OVMF_LOCAL_VARS) $(SWTPM_STATE)
	$(SWTPM) $(SWTPM_ARGS)
	$(QEMU_RUN_IMAGE)

test: clean-image run-sign grow run-encrypt run-verify

clean-image:
	rm -f $(IMAGE_FILE)

clean:
	rm -rf $(BUILD_DIR)/ *~ src/*~ src/imprint/*~
