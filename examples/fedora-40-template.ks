# SPDX-License-Identifier: LGPL-2.1-or-later
# SPDX-FileCopyrightText: 2023 Red Hat

# Use text install
text

# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

%packages
@^minimal-environment

-dracut-config-rescue

kernel-uki-virt
uki-direct

# WALinuxAgent
cloud-init
cloud-utils-growpart

NetworkManager-cloud-setup

tpm2-tools
efibootmgr
cryptsetup

%end

# Do not run the Setup Agent on first boot
firstboot --disable

# Generated using Blivet version 3.8.1
ignoredisk --only-use=vda
# Partition clearing information
clearpart --none --initlabel
# Disk partitioning information
part /boot/efi --fstype="efi" --ondisk=vda --size=1007 --fsoptions="umask=0077,shortname=winnt"
part / --fstype="ext4" --ondisk=vda --size=3087

# System services
services --enabled="sshd,NetworkManager,nm-cloud-setup.service,nm-cloud-setup.timer,cloud-init,cloud-init-local,cloud-config,cloud-final,kernel-bootcfg-boot-successful"
# add waagent if needed

# System timezone
timezone Etc/UTC --utc

# Root password
rootpw --allow-ssh redhatHV

# Poweroff
poweroff

%post --erroronfail
# Set 'discoverable' root partition GUID for systemd-gpt-auto-generator
sfdisk --part-type /dev/vda 2 4F68BCE3-E8CD-4DB1-96E7-FBCAF984B709

# remove 'standard' kernel
rpm -e kernel-core kernel-modules kernel

# re-create modules.dep
depmod `rpm -q --queryformat %{VERSION}-%{RELEASE}.%{ARCH} kernel-uki-virt`

# configure UKI bootup
kernel-bootcfg --boot-order 0 --add-uki "/boot/efi/EFI/Linux/"`cat /etc/machine-id`"-"`rpm -q --queryformat %{VERSION}-%{RELEASE} kernel-uki-virt`".x86_64.efi" --title `rpm -q --queryformat %{VERSION}-%{RELEASE} kernel-uki-virt`
kernel-bootcfg --update-csv

# Fstrim root
fstrim -v / ||:

%end
