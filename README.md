IMPRINT: Image Provisioning with Introspectable Trust
=====================================================

The IMPRINT tool is a mechanism for provisioning an encrypted
virtual machine disk image, from a plain text template, with
the ability introspect the encrypted image to establish trust
in the provisioning process.

This is licensed under the terms of the LGPL-2.1-or-later

Usage
-----

The first step is a create (or acquire) a compatible plain
text image. This is a disk image, typically supplied by an
OS vendor, that has two partitions:

* EFI System Partition (ESP)

  GPT GUID == c12a7328-f81f-11d2-ba4b-00a0c93ec93b

* Root filesystem

  GPT GUID == 4f68bce3-e8cd-4db1-96e7-fbcaf984b709

and is configured such that it can boot under EFI, with
SecureBoot and partition auto-discovery to find the root
FS.

### Signing the image

With the pristine image template ready, the next step is for the
image vendor to sign the contents, which requires the ESP partition
to be mounted, and root partition unmounted:

``
  imprint sign --esp-dir /path/to/esp --root-dev /path/to/root-partition
``

The signing process creates an ECDSA signature that covers all the
files in the ESP, along with the raw contents of the root filesystem
partition.

It is critical that after signing, no attempt is made to mount the
root filesystem, because the mere act of mounting usually changes
filesystem metadata and thus invalidates the signature. IOW, signing
must be the very last action that modifies the disk image before it
is shipped to users for deployment.

For the adhoc developer testing, the 'run-sign' make target will spawn
a local QEMU with a dummy image and sign it:

``
  make vm-sign
``

### Instantiating the image

The end user receives the image template and wants to instantiate
it. It is permitted to enlarge the disk image, growing the root
filesystem partition in the process. It is also permitted to
apply LUKS encryption to the root filesystem.

After enlargement and encryption are complete, but critically
before mounting the root filesystem, the signature must be verified:

``
  imprint sign --esp-dir /path/to/esp --root-dev /path/to/root-partition
``

Note that if the image was encrypted, then /path/to/root-partition
should point to the open LUKS volume, not the raw partition.

For adhoc developer testing the 'grow' make target will enlarge the
disk image and resize the last partition. It will grow by 32 MB
which is the minimum space needed to apply LUKS encryption, but a
larger size can be requested:

``
  make grow GROW_MB=500
``

The 'run-encrypt' and 'run-verify' commands can then be used to
spawn a local QEMU instance that performs encryption and verification:

``
  make run-encrypt
  make run-verify
``

### Example image creation

The makefile for developer usage will create a dummy disk image that
contains an empty ESP and root filesystem. This is fast for adhoc
testing. It is possible to create a suitable real world disk iamge
though and use that by setting the ``IMAGE_TEMPLATE`` make variable.

An example kickstart file for creating such a compatible
image with Fedora is available under the ``examples`` dir,
and can be created using the ``virt-install`` tool:

```
  virt-install \
    --virt-type kvm \
    --os-variant fedora-rawhide \
    --arch x86_64 \
    --boot uefi \
    --name f40 \
    --memory 8192 \
    --disk bus=virtio,size=4
    --network bridge=br0 \
    --location https://download.fedoraproject.org/pub/fedora/linux/development/rawhide/Everything/x86_64/os/ \
    --initrd-inject=fedora-40-template.ks \
    --extra-args "console=ttyS0 inst.ks=file:/fedora-40-template.ks" \
    --nographics \
    --noreboot \
    --transient
```

The resulting file will be in one of two possible locations

  * `/var/lib/libvirt/images/f40.qcow2`
  * `$HOME/VirtualMachines/f40.qcow2`

Building
--------

The IMPRINT project is written in Rust and primarily uses
Cargo for its build process. Some of the crates, however,
have dependencies on native packages

* libcryptsetup-rs

  Requires: https://gitlab.com/cryptsetup/cryptsetup

  ie cryptsetup-devel on Fedora/RHEL

* tss-esapi

  Requires: https://github.com/tpm2-software/tpm2-tss

  ie tpm2-tss-devel on Fedora/RHEL

The adhoc makefile targets will also use a number of
additional tools:

* sfdisk
* guestfish

These are not required in real world production deployments
