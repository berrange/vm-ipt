// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2023 Red Hat

use std::path::PathBuf;

use anyhow::{bail, Context, Result};

use crate::imprint::blkid::is_luks;
use crate::imprint::progress::get_progress_bars;
use crate::imprint::signature::Signature;
use crate::imprint::tpm::{measure_signature, Quote};

pub fn run(root_dev: &PathBuf, root_size: Option<u64>, esp_dir: &PathBuf) -> Result<()> {
    let (_progress, operation, mut completion) = get_progress_bars();

    operation.set_message("Checking image encryption status");
    if is_luks(&root_dev).context("cannot check root filesystem encryption status")? {
        bail!("Root filesystem is already encrypted");
    }

    let sig = Signature::from_file(esp_dir).context("cannot load image signature")?;

    if Quote::exists(esp_dir) {
        operation.set_message("Verifying existing quote");

        let quote = Quote::from_file(esp_dir).context("cannot load TPM quote")?;
        quote.verify(&sig).context("cannot verify TPM quote")?;

        measure_signature(&sig)?;
    } else {
        operation.set_message("Verifying image");
        sig.verify(esp_dir, root_dev, root_size, &mut completion)
            .context("cannot verify image signature")?;

        operation.set_message("Creating verification quote");
        let quote = Quote::from_signature(&sig).context("cannot create TPM quote")?;
        measure_signature(&sig)?;
        quote.save(esp_dir).context("cannot save TPM quote")?;
    }
    Ok(())
}
