// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2023 Red Hat

use std::path::PathBuf;

use anyhow::{bail, Context, Result};

use crate::imprint::blkid::is_luks;
use crate::imprint::progress::get_progress_bars;
use crate::imprint::signature::Signature;

pub fn run(
    root_dev: &PathBuf,
    root_size: Option<u64>,
    esp_dir: &PathBuf,
    replace: bool,
) -> Result<()> {
    let (_progress, operation, mut completion) = get_progress_bars();

    operation.set_message("Checking image encryption status");
    if is_luks(&root_dev).context("cannot check root filesystem encryption status")? {
        bail!("Root filesystem is already encrypted");
    }

    operation.set_message("Signing image");
    let sig = Signature::from_host(esp_dir, root_dev, root_size, replace, &mut completion)
        .context("cannot create image signature")?;
    sig.save(esp_dir).context("cannot save image signature")?;

    Ok(())
}
