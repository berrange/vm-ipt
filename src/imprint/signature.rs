// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2023 Red Hat

use anyhow::{bail, Result};
use base64::engine::{general_purpose::STANDARD as base64, Engine};
use bytesize::ByteSize;
use indicatif::ProgressBar;

use crate::imprint::progress::{completion_style_sign, completion_style_verify};
use digest::Digest;
use ed25519_dalek::{Signature as EDSignature, SigningKey, VerifyingKey};
use rand::rngs::OsRng;
use serde::de;
use serde::{Deserialize, Serialize, Serializer};
use serde_json;
use sha2::{Sha256, Sha512};
use std::cmp::min;
use std::convert::TryInto;
use std::fs;
use std::fs::create_dir_all;
use std::fs::File;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::iter::IntoIterator;
use std::path::Path;
use std::path::PathBuf;
use unicode_segmentation::UnicodeSegmentation;
use walkdir::WalkDir;

const DIGEST_READ_BUFSIZE: usize = ByteSize::mb(1).as_u64() as usize;

fn get_files(dir: &PathBuf) -> Result<Vec<PathBuf>> {
    // NB, walk ordering stability is critical to preserving
    // the signature validity

    let mut filter = dir.clone();
    filter.push("imprint");

    Ok(WalkDir::new(dir)
        .sort_by_file_name()
        .contents_first(false)
        .into_iter()
        .map(|e| e.unwrap().into_path())
        .filter(|e| !e.starts_with(filter.clone()) && e.is_file())
        .collect())
}

fn serialize_datasig<S>(x: &EDSignature, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let b64 = base64.encode(x.to_bytes());
    s.serialize_str(&b64)
}

fn deserialize_datasig<'de, D>(d: D) -> Result<EDSignature, D::Error>
where
    D: de::Deserializer<'de>,
{
    use serde::de::Error;
    let res = String::deserialize(d)
        .and_then(|string| {
            base64
                .decode(&string)
                .map_err(|err| Error::custom(err.to_string()))
        })
        .map(|bytes| EDSignature::from_slice(&bytes));
    res?.map_err(|err| Error::custom(err.to_string()))
}

fn serialize_verifier<S>(x: &VerifyingKey, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let b64 = base64.encode(&x.to_bytes());
    s.serialize_str(&b64)
}

fn deserialize_verifier<'de, D>(d: D) -> Result<VerifyingKey, D::Error>
where
    D: de::Deserializer<'de>,
{
    use serde::de::Error;
    let res = String::deserialize(d)
        .and_then(|string| {
            base64
                .decode(&string)
                .map_err(|err| Error::custom(err.to_string()))
        })
        .map(|bytes| {
            let b: [u8; 32] = bytes.try_into().unwrap();
            VerifyingKey::from_bytes(&b)
        });
    res?.map_err(|err| Error::custom(err.to_string()))
}

#[derive(Serialize, Deserialize)]
pub struct Signature {
    #[serde(
        serialize_with = "serialize_datasig",
        deserialize_with = "deserialize_datasig"
    )]
    datasig: EDSignature,

    #[serde(
        serialize_with = "serialize_verifier",
        deserialize_with = "deserialize_verifier"
    )]
    verifier: VerifyingKey,

    root_size: u64,
}

fn new_signing_key() -> SigningKey {
    let mut rng = OsRng {};
    SigningKey::generate(&mut rng)
}

fn digest_one(
    dig: &mut Sha512,
    path: &Path,
    ftype: &str,
    size: Option<u64>,
    completion: &mut ProgressBar,
) -> Result<u64> {
    let fb = match path.to_str() {
        Some(s) => s,
        None => bail!("Invalid path {}", path.display()),
    };
    let mut fh = match File::open(path) {
        Ok(f) => f,
        Err(e) => bail!("cannot open {}: {}", path.display(), e),
    };

    /* Block devs report 0 for fh.metadata().len() */
    let len = fh.seek(SeekFrom::End(0))?;
    fh.seek(SeekFrom::Start(0))?;

    let mut remaining = size.unwrap_or(len);

    let mut fbgr: String = fb
        .graphemes(true)
        .take(20)
        .flat_map(|s| s.chars())
        .collect();
    if fbgr != fb {
        fbgr += "…";
    }

    completion.set_message(format!("{}: {}", ftype, fbgr));
    completion.set_length(remaining);
    completion.set_position(0);

    let mut buf: [u8; DIGEST_READ_BUFSIZE] = [0; DIGEST_READ_BUFSIZE];
    let mut k = 0;
    while remaining > 0 {
        let want = min(DIGEST_READ_BUFSIZE as u64, remaining);
        let n = fh.read(&mut buf[0..want as usize])? as u64;
        if n == 0 {
            break;
        }
        k += n;
        remaining -= n;
        dig.update(&buf[0..n as usize]);
        completion.set_position(k as u64);
    }

    Ok(k)
}

fn build_digest(
    espdir: &PathBuf,
    root_dev: &PathBuf,
    root_size: Option<u64>,
    completion: &mut ProgressBar,
) -> Result<(Sha512, u64)> {
    let files = get_files(espdir)?;

    let mut dig: Sha512 = Sha512::new();
    for e in files {
        /* Mere existance of a file can conceivably change
         * behaviour. Include the filename, so that zero
         * length files have influence on the signature
         */
        let pathbytes = match e.as_path().to_str() {
            Some(s) => s,
            None => bail!("Invalid path {}", e.as_path().display()),
        };
        dig.update(pathbytes);

        digest_one(&mut dig, &e.as_path(), "File", None, completion)?;
    }

    let actual_root_size = digest_one(&mut dig, root_dev, "Partition", root_size, completion)?;

    Ok((dig, actual_root_size))
}

fn sig_dir(espdir: &PathBuf) -> PathBuf {
    let mut dir = espdir.clone();
    dir.push("imprint");
    dir
}

fn sig_file(espdir: &PathBuf) -> PathBuf {
    let mut file = sig_dir(espdir);
    file.push("signature.json");
    file
}

impl Signature {
    pub fn from_file(espdir: &PathBuf) -> Result<Signature> {
        let file = sig_file(espdir);
        if !file.exists() {
            bail!("image is not signed ({} does not exist)", file.display())
        }
        let data = match fs::read_to_string(sig_file(espdir)) {
            Ok(d) => d,
            Err(e) => bail!("cannot read signature {}: {}", file.display(), e),
        };
        let sig: Signature = serde_json::from_str(&data)?;
        Ok(sig)
    }

    pub fn from_host(
        espdir: &PathBuf,
        root_dev: &PathBuf,
        root_size: Option<u64>,
        replace: bool,
        completion: &mut ProgressBar,
    ) -> Result<Signature> {
        let file = sig_file(&espdir);
        if file.exists() && !replace {
            bail!("image is already signed ({} exists)", file.display())
        }
        let key = new_signing_key();
        let (dig, actual_root_size) = build_digest(
            &espdir,
            root_dev,
            root_size,
            &mut completion_style_sign(completion.clone()),
        )?;
        Ok(Signature {
            datasig: key.sign_prehashed(dig, None)?,
            verifier: key.verifying_key(),
            root_size: actual_root_size,
        })
    }

    pub fn verify(
        &self,
        espdir: &PathBuf,
        root_dev: &PathBuf,
        root_size: Option<u64>,
        completion: &mut ProgressBar,
    ) -> Result<()> {
        let actual_root_size = match root_size {
            None => self.root_size,
            Some(s) => s,
        };
        let (dig, _actual_root_size) = build_digest(
            &espdir,
            root_dev,
            Some(actual_root_size),
            &mut completion_style_verify(completion.clone()),
        )?;

        self.verifier.verify_prehashed(dig, None, &self.datasig)?;
        Ok(())
    }

    pub fn save(&self, espdir: &PathBuf) -> Result<()> {
        let json = serde_json::to_string(self)?;

        create_dir_all(sig_dir(espdir))?;
        fs::write(sig_file(espdir), json)?;
        Ok(())
    }

    pub fn digest(&self) -> Result<[u8; 32]> {
        let json = serde_json::to_string(self)?;

        return Ok(Sha256::digest(json).into());
    }
}
