// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2023 Red Hat

use std::convert::TryFrom;
use std::fs;
use std::fs::create_dir_all;
use std::path::PathBuf;
use std::str::FromStr;

use anyhow::{anyhow, bail, Result};
use base64::engine::{general_purpose::STANDARD as base64, Engine};
use digest::Digest;
use serde::de;
use serde::{Deserialize, Serialize, Serializer};
use serde_json;
use sha2::Sha256;
use tss_esapi::{
    abstraction::{
        ak::{create_ak, load_ak},
        ek::create_ek_object,
    },
    attributes::SessionAttributesBuilder,
    constants::SessionType,
    handles::{KeyHandle, PcrHandle, PersistentTpmHandle, TpmHandle},
    interface_types::{
        algorithm::{AsymmetricAlgorithm, HashingAlgorithm, SignatureSchemeAlgorithm},
        dynamic_handles::Persistent,
        resource_handles::Provision,
        session_handles::AuthSession,
    },
    structures::{
        Attest, AttestInfo, Data, Digest as TPMDigest, DigestValues, PcrSelectionListBuilder,
        PcrSlot, Signature as TPMSignature, SignatureScheme, SymmetricDefinition,
    },
    tcti_ldr::TctiNameConf,
    traits::{Marshall, UnMarshall},
    Context,
};

use crate::imprint::signature::Signature;

// "It is a very interesting number"
const AK_HANDLE: u32 = u32::from_be_bytes([0x81, 0x00, 0x17, 0x29]);

const SIGNATURE_MEASURE_PCR: PcrHandle = PcrHandle::Pcr15;

fn serialize_attest<S>(x: &Attest, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    use serde::ser::Error;
    let b64 = base64.encode(x.marshall().map_err(|err| Error::custom(err.to_string()))?);
    s.serialize_str(&b64)
}

fn deserialize_attest<'de, D>(d: D) -> Result<Attest, D::Error>
where
    D: de::Deserializer<'de>,
{
    use serde::de::Error;
    let res = String::deserialize(d)
        .and_then(|string| {
            base64
                .decode(&string)
                .map_err(|err| Error::custom(err.to_string()))
        })
        .map(|bytes| Attest::unmarshall(&bytes));
    res?.map_err(|err| Error::custom(err.to_string()))
}

fn serialize_signature<S>(x: &TPMSignature, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    use serde::ser::Error;
    let b64 = base64.encode(&x.marshall().map_err(|err| Error::custom(err.to_string()))?);
    s.serialize_str(&b64)
}

fn deserialize_signature<'de, D>(d: D) -> Result<TPMSignature, D::Error>
where
    D: de::Deserializer<'de>,
{
    use serde::de::Error;
    let res = String::deserialize(d)
        .and_then(|string| {
            base64
                .decode(&string)
                .map_err(|err| Error::custom(err.to_string()))
        })
        .map(|bytes| TPMSignature::unmarshall(&bytes));
    res?.map_err(|err| Error::custom(err.to_string()))
}

#[derive(Serialize, Deserialize)]
pub struct Quote {
    #[serde(
        serialize_with = "serialize_attest",
        deserialize_with = "deserialize_attest"
    )]
    attest: Attest,

    #[serde(
        serialize_with = "serialize_signature",
        deserialize_with = "deserialize_signature"
    )]
    signature: TPMSignature,
}

// Need to select PCRs that can establish the execution context
// in which the quote was created:
//   - Slot 7  -> to prove that SecureBoot is enabled, and
//                the vendor certs used to validate the UKI
//   - Slot 15 -> to tie the quote to the LUKS volume key
//                which acts as the identity of the encrypted
//                root disk for this VM
//
// XXX we want 'pcrphase' to prove we are in the initrd, but
// we don't want to use Slot 11, as that contanis the raw
// kernel/initrd authenticode values which change on every
// update :-( Should request to add 'pcrphase' to slot 15 too
fn quote_sealing_pcrs() -> [PcrSlot; 2] {
    return [PcrSlot::Slot7, PcrSlot::Slot15];
}

fn new_context() -> Result<Context> {
    let config = TctiNameConf::from_str("device:/dev/tpm0")?;
    let context = Context::new(config)?;

    Ok(context)
}

fn fetch_ak(context: &mut Context) -> Result<KeyHandle> {
    let pakh = PersistentTpmHandle::new(AK_HANDLE)?;

    let ak = context.tr_from_tpm_public(TpmHandle::Persistent(pakh))?;

    Ok(ak.into())
}

fn store_ak(context: &mut Context, ek: KeyHandle) -> Result<KeyHandle> {
    let akres = create_ak(
        context,
        ek,
        HashingAlgorithm::Sha256,
        SignatureSchemeAlgorithm::RsaPss,
        None,
        None,
    )?;

    let pakh = PersistentTpmHandle::new(AK_HANDLE)?;
    let ak = load_ak(context, ek, None, akres.out_private, akres.out_public)?;

    let hsession =
        hmac_session(context).map_err(|e| anyhow!("Unable to start HMAC session: {}", e))?;
    let pak = context
        .execute_with_session(Some(hsession), |ctx| {
            ctx.evict_control(Provision::Owner, ak.into(), Persistent::Persistent(pakh))
        })
        .map_err(|e| anyhow!("Unable to persist new AK: {}", e))?;

    Ok(pak.into())
}

fn get_ak(context: &mut Context) -> Result<KeyHandle> {
    let ak = match fetch_ak(context) {
        Ok(h) => h,
        Err(_e) => {
            let ek = create_ek_object(context, AsymmetricAlgorithm::Rsa, None)?;
            store_ak(context, ek)?
        }
    };

    Ok(ak)
}

fn hmac_session(context: &mut Context) -> Result<AuthSession> {
    let session = context
        .start_auth_session(
            None,
            None,
            None,
            SessionType::Hmac,
            SymmetricDefinition::AES_128_CFB,
            HashingAlgorithm::Sha256,
        )?
        .ok_or(anyhow!("no auth session present"))?;

    let (session_attributes, session_attributes_mask) = SessionAttributesBuilder::new().build();
    context.tr_sess_set_attributes(session, session_attributes, session_attributes_mask)?;

    Ok(session)
}

pub fn measure_signature(sig: &Signature) -> Result<()> {
    let mut context = new_context()?;
    let mut vals = DigestValues::new();
    vals.set(
        HashingAlgorithm::Sha256,
        TPMDigest::try_from(sig.digest()?.to_vec())?,
    );

    let hsession =
        hmac_session(&mut context).map_err(|e| anyhow!("Unable to start HMAC session: {}", e))?;
    context
        .execute_with_session(Some(hsession), |ctx| {
            ctx.pcr_extend(SIGNATURE_MEASURE_PCR, vals)
        })
        .map_err(|e| anyhow!("Unable to measure signature into PCR: {}", e))?;

    Ok(())
}

fn quote_dir(espdir: &PathBuf) -> PathBuf {
    let mut dir = espdir.clone();
    dir.push("imprint");
    dir
}

fn quote_file(espdir: &PathBuf) -> PathBuf {
    let mut file = quote_dir(espdir);
    file.push("quote.json");
    file
}

impl Quote {
    pub fn exists(espdir: &PathBuf) -> bool {
        let file = quote_file(espdir);
        file.exists()
    }

    pub fn from_file(espdir: &PathBuf) -> Result<Quote> {
        let file = quote_file(espdir);
        if !file.exists() {
            bail!("signature is not quote ({} does not exist)", file.display())
        }
        let data = match fs::read_to_string(file.clone()) {
            Ok(d) => d,
            Err(e) => bail!("cannot read quote {}: {}", file.display(), e),
        };
        let quote: Quote = serde_json::from_str(&data)?;
        Ok(quote)
    }

    pub fn from_signature(sig: &Signature) -> Result<Quote> {
        let mut context = new_context()?;

        let ak =
            get_ak(&mut context).map_err(|e| anyhow!("Unable to create and load AK: {}", e))?;

        let pcrs = PcrSelectionListBuilder::new()
            .with_selection(HashingAlgorithm::Sha256, &quote_sealing_pcrs())
            .build()?;

        let hsession = hmac_session(&mut context)
            .map_err(|e| anyhow!("Unable to start HMAC session: {}", e))?;
        let qdata = sig.digest()?;

        let (attest, signature) = context
            .execute_with_session(Some(hsession), |ctx| {
                ctx.quote(
                    ak,
                    Data::try_from(qdata.to_vec())?,
                    SignatureScheme::Null,
                    pcrs.clone(),
                )
            })
            .map_err(|e| anyhow!("Unable to quote signature: {}", e))?;

        Ok(Quote { attest, signature })
    }

    pub fn save(&self, espdir: &PathBuf) -> Result<()> {
        let json = serde_json::to_string(self)?;

        create_dir_all(quote_dir(espdir))?;
        fs::write(quote_file(espdir), json)?;
        Ok(())
    }

    pub fn verify(&self, sig: &Signature) -> Result<()> {
        let mut context = new_context()?;

        let ak =
            fetch_ak(&mut context).map_err(|e| anyhow!("Unable to fetch persistent AK: {}", e))?;

        let attested = self.attest.marshall()?;
        let attested_digest: [u8; 32] = Sha256::digest(attested).into();

        // Prove: the signature was created by the AK that exists in
        //        this machine's TPM nvram
        context
            .verify_signature(
                ak,
                TPMDigest::try_from(attested_digest.to_vec())?,
                self.signature.clone(),
            )
            .map_err(|e| anyhow!("quoted signature does not verify: {}", e))?;

        let pcrs = PcrSelectionListBuilder::new()
            .with_selection(HashingAlgorithm::Sha256, &quote_sealing_pcrs())
            .build()?;

        let qdata = Data::try_from(sig.digest()?.to_vec())?;

        // Prove: we have a sufficiently broad set of PCRs selected
        //        to establish the point in boot that we expect to be in
        if self.attest.extra_data().value() != qdata.value() {
            bail!(
                "quoted signature {:x?} does not match expected signature {:x?}",
                self.attest.extra_data().value(),
                qdata.value(),
            );
        }

        let (_counter, _pcrs, pcrdigests) = context.pcr_read(pcrs.clone())?;

        let mut digest = Sha256::new();
        for pcrdig in pcrdigests.value() {
            digest.update(pcrdig.value());
        }
        let expect_pcrdig: [u8; 32] = digest.finalize().into();

        // Prove: the quote was created at the same point in boot
        //        that we are in when validating
        match self.attest.attested() {
            AttestInfo::Quote { info } => {
                if info.pcr_digest().value() != expect_pcrdig {
                    bail!(
                        "quoted PCR digest {:x?} does not match expected digest {:x?}",
                        info.pcr_digest().value(),
                        expect_pcrdig
                    );
                }
                if *info.pcr_selection() != pcrs {
                    bail!("quoted PCR selection does not match expected selection",);
                }
            }
            _ => bail!("unexpected attestation type"),
        };

        // Result: we know that the image signature was verified when
        //         the disk was first encrypted before first mount

        Ok(())
    }
}
