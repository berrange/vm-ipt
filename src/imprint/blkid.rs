// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2023 Red Hat

use anyhow::Result;
use libblkid_rs::{BlkidProbe, BlkidProbeRet, BlkidSublks, BlkidSublksFlags};
use std::path::PathBuf;

pub fn is_luks(dev: &PathBuf) -> Result<bool> {
    let mut probe = BlkidProbe::new_from_filename(&dev)?;
    probe.enable_superblocks(true)?;
    probe.enable_partitions(false)?;
    let flags: Vec<BlkidSublks> = vec![BlkidSublks::Badcsum, BlkidSublks::Type];
    probe.set_superblock_flags(BlkidSublksFlags::new(flags))?;

    loop {
        match probe.do_probe()? {
            BlkidProbeRet::Success => {
                if probe.lookup_value("TYPE")? == "crypto_LUKS" {
                    return Ok(true);
                }
            }
            BlkidProbeRet::Done => break,
        };
    }

    return Ok(false);
}
