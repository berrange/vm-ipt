// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2023 Red Hat

use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use std::time::Duration;

pub fn get_progress_bars() -> (MultiProgress, ProgressBar, ProgressBar) {
    let progress = MultiProgress::new();
    let operation = progress.add(ProgressBar::new(0));
    let mut completion = progress.add(ProgressBar::new(0));

    operation.set_style(
        ProgressStyle::with_template("  ▶{spinner}◀ {msg:25} [{elapsed_precise}]")
            .unwrap()
            .tick_chars("🕛🕧🕐🕜🕑🕝🕒🕞🕓🕟🕔🕠🕕🕡🕖🕢🕗🕣🕘🕤🕙🕥🕚🕦"),
    );
    operation.enable_steady_tick(Duration::from_millis(250));

    completion = completion_style_generic(completion);

    (progress, operation, completion)
}

pub fn completion_style(progress: ProgressBar, chars: &str) -> ProgressBar {
    progress.set_style(
        ProgressStyle::with_template("  ▶{bar:40.cyan/blue}◀ {msg:30} {percent}%")
            .unwrap()
            .progress_chars(chars),
    );
    progress
}

pub fn completion_style_generic(progress: ProgressBar) -> ProgressBar {
    completion_style(progress, "🔹🔨🔵")
}

pub fn completion_style_sign(progress: ProgressBar) -> ProgressBar {
    completion_style(progress, "✅📝⭕")
}

pub fn completion_style_verify(progress: ProgressBar) -> ProgressBar {
    completion_style(progress, "✅🔬⭕")
}
