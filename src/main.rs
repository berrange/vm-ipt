// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2023 Red Hat

use std::env::set_var;
use std::path::PathBuf;
use std::process;

use anyhow::anyhow;
use clap::{Parser, Subcommand};

mod imprint;
mod sign;
mod verify;

#[derive(Parser)]
struct Imprint {
    #[command(subcommand)]
    command: Option<ImprintCommand>,
}

fn default_root_dev() -> PathBuf {
    let mut p = PathBuf::new();
    p.push("dev");
    p.push("gpt-auto-root");
    p
}

fn default_esp_dir() -> PathBuf {
    let mut p = PathBuf::new();
    p.push("boot");
    p.push("efi");
    p
}

#[derive(Subcommand)]
enum ImprintCommand {
    Sign {
        /// The device path for the partition containing the root filesystem
        #[arg(short, long, value_name = "DEVICE-PATH", default_value=default_root_dev().into_os_string())]
        root_dev: PathBuf,

        /// The size of the root partition to sign
        #[arg(short = 's', long, value_name = "BYTES")]
        root_size: Option<u64>,

        /// The path for the mount point of the EFI system partition (ESP)
        #[arg(short, long, value_name = "MOUNT-POINT", default_value=default_esp_dir().into_os_string())]
        esp_dir: PathBuf,

        /// Replace existing signature if it already exists
        #[arg(short, long)]
        force: bool,
    },
    Verify {
        /// The device path for the partition containing the root filesystem
        #[arg(short, long, value_name = "DEVICE-PATH", default_value=default_root_dev().into_os_string())]
        root_dev: PathBuf,

        /// The size of the root partition to verify
        #[arg(short = 's', long, value_name = "BYTES")]
        root_size: Option<u64>,

        /// The path for the mount point of the EFI system partition (ESP)
        #[arg(short, long, value_name = "MOUNT-POINT", default_value=default_esp_dir().into_os_string())]
        esp_dir: PathBuf,
    },
}

fn main() {
    let imprint = Imprint::parse();

    if true {
        set_var("TSS2_LOG", "all+NONE");
    }

    let err = match &imprint.command {
        Some(ImprintCommand::Sign {
            root_dev,
            root_size,
            esp_dir,
            force,
        }) => sign::run(root_dev, *root_size, esp_dir, *force).err(),
        Some(ImprintCommand::Verify {
            root_dev,
            root_size,
            esp_dir,
        }) => verify::run(root_dev, *root_size, esp_dir).err(),
        None => Some(anyhow!("no command specified")),
    };

    match err {
        Some(e) => match e.source() {
            Some(cause) => {
                eprintln!("🔥 {}: {} 🔥", e.to_string(), cause.to_string());
                process::exit(1);
            }
            None => {
                eprintln!("🔥 {} 🔥", e.to_string());
                process::exit(1);
            }
        },
        None => {
            eprintln!("");
            process::exit(0);
        }
    }
}
